import React from 'react'
import {Container, Nav, Navbar} from "react-bootstrap"
import logo from '../logo.png'

export default function Header() {
    return(
        <>
            <Navbar bg="dark" expand="lg" fixed>
                <Container>
                    <Navbar.Brand href="#">
                        <div className="h-100 d-flex align-items-center">
                            <div>
                                <img src={logo} className="App-logo" alt="logo" />
                            </div>
                            <div style={{paddingLeft: "15px"}}>
                                <span>LAMIRAND Cyril</span>
                            </div>
                        </div>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}