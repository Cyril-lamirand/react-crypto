import React from 'react'
import axios from 'axios'
import {useState, useEffect} from "react"

export default function Api() {

    const [dataFromBinance, setDataFromBinance] = useState(null)

    useEffect(() => {
        if (dataFromBinance === null) {
            axios.get("https://api.binance.com/api/v3/ticker/price")
                .then(res => {
                    setDataFromBinance(res.data)
                })
                .catch(error => {
                    console.log("Axios : " + error)
                })
        }
    }, [dataFromBinance])

    return (
        <>
            <div className="container">
                {
                    dataFromBinance ?
                        <>
                            <div className="row mt-2">
                                {
                                    dataFromBinance.map(function(d, idx){
                                        return (
                                            <div key={idx} className="col-12 col-sm-12 col-md-3 col-lg-2 mt-2 mb-2">
                                                <div className="bg-card pl-2 pr-2 text-center">
                                                    <div className="pt-2">
                                                        <h6>{d.symbol}</h6>
                                                    </div>
                                                    <div className="pt-2 pb-2">
                                                        <span>{d.price}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </>
                        :
                        <>
                            <div className="text-center">
                                <h1>Chargement...</h1>
                                <p>Veuillez patientez.</p>
                            </div>
                        </>
                }
            </div>

        </>
    )
}