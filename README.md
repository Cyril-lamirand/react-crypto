# react-crypto

React JS | Projet IIM A4 - Culture DevOps

## Environnement

VPS Ovh (Ubuntu 21) => http://151.80.152.40:8080/ avec une image Docker : NGINX

Url du projet => http://151.80.152.40:80/

Linter : ESLint

## Configuration

J'ai configuré un "Runner" GitLab sur mon VPS.

J'ai setup la configuration CI/CD dans le fichier :

`gitlab-ci.yml`

## Problème(s) Rencontré(s)

Si dans la section script du fichier "gitlab-ci.yml" j'indique la commande :

`npm run start`

J'ai bien accès au projet React à l'adresse :

http://151.80.152.40:3000/ 

Mais le "Job" ne s'arrête pas. Il continue son execution pendant 1h avant d'échouer (et donc l'URL n'est plus accessible par la suite)...

## Solution trouvée

J'ai remplacer l'image Docker NGINX par "le vrai" NGINX. 

J'ai ensuite édité le fichier "default" situé =

`/etc/nginx/sites-enabled/` 
 
 Avec la commande :

`sudo nano default`

Afin que ce dernier écoute le répertoire :

`/var/www/html/react-crypto/build`

Et ça fonctionne ! :-)
